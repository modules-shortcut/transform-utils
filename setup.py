from setuptools import setup, find_packages

# read requirements files
with open('requirements.txt', 'r') as f:
    requirements = f.read().splitlines()

setup(
    name="transform-utils",
    version="1.0.1",
    install_requires=requirements,
    author="Rumah Logic",
    author_email="support@rumahlogic.com",
    description="this package for utils",
    long_description=open('README.md').read(),
    long_description_content_type="text/markdown",
    url="https://gitlab.com/modules-shortcut/transform-utils",
    packages=find_packages(),
)