from .transform import Transform, TransformDocx, Merge

  
# param #1 = JSON Path list data yang ingin di olah jadi tabel
# param #2 = Separator untuk JSON Path
# param #3~ = kolom baru untuk tabel
# fungsi yang dapat dilakukan yaitu [average,sum,count,max,min]
# penggunaan fungsi dapat dilakukan dengan menambahkannya pada awal kolom (json path) dengan pemisah (|)
# contoh count|friends = count sebagai fungsi dan friends sebagai JSON path kolom yang ingin dipilih
# jika value yang ingin dilakukan fungsi berupa array object dapat disematkan JSON path setelah array object tersebut dengan pemisah (:) pada nama fungsi
# contoh average:child.score|scores.count = average adalah nama fungsi, child.score adalah JSON path setelah array object, scores.count adalah JSON path lokasi array object
# jika ingin mengganti nama kolom dapat dilakukan dengan menambahkan alias setelah JSON path dengan pemisah (:)
# contoh count|friends:jumlah_teman = count adalah nama fungsi, friends adalah JSON path array object, jumlah_teman adalah alias yang ingin digunakan untuk mengganti kolom friends

# new_obj = [
#   'name:nama', 
#   'age:umur', 
#   'count|friends:jumlah_teman', 
#   'average:child.score|scores.count:rata_rata'
# ]
# cfg = Config('data', '.', *new_obj)
# print(cfg.parse(ex7))

# data =  Transform('', ".") \
#           .parse(ex8) \
#           .average('report_type_id', 'data') \
#           .ceil('data') \
#           .floor('data') \
#           .concat('report_type_id', 'data', ' - ') \
#           .len('concat') \
#           .replace('concat', 2, 1, 'tes ApA saJA') \
#           .upper('replace') \
#           .lower('replace') \
#           .title('replace') \
#           # .transform('report_type_id', 'status', 'data') \
#           # .min('report_type_id', 'data')
#           # .count('report', 'jumlah') \
#           # .sort('-jumlah')

# print(data.to_json())

# new_data = cfg.parse(ex8)

# new_table
# new_table
# print(transform)
