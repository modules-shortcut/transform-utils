import json
import re
import math
import gspread
import os
import pandas as pd
import locale
import time
import dateparser

from functools          import reduce
from io                 import StringIO
from google.oauth2      import service_account
from datetime           import datetime
from dateutil           import parser
from collections        import defaultdict


class DateConverter:
    def __init__(self):
        self.iso_formats = [
            r"%Y-%m-%dT%H:%M:%S.%fZ",  # Format with milliseconds and 'Z'
            r"%Y-%m-%dT%H:%M:%S",      # Format with 'T' separator
            r"%Y-%m-%dT%H:%M:%SZ",     # ISO 8601 without milliseconds
        ]
        
        self.other_formats = [
            r"%A, %d %B %Y %H:%M %Z",      # Indonesian
            r"%Y-%m-%d %H:%M:%S",      # Date and time with space separator
            r"%Y-%m-%d",               # Date only
            r"%m/%d/%Y",               # USA
            r"%B %d, %Y",              # USA long
            r"%m-%d-%Y",               # USA alternate
            r"%d/%m/%Y",               # European
            r"%d %B %Y",               # European long
            r"%d-%m-%Y",               # European alternate
            r"%Y年%m月%d日",           # Chinese, Japanese
            r"%Y/%m/%d",               # Korean
            r"%d.%m.%Y",               # Russian
            r"%d de %B de %Y",         # Spanish
            r"%d-%m-%Y",               # Brazil, Arabic, Australia
            r"%d-%m-%Y",               # India, UK, Australia
        ]
        
        # Combine all formats
        self.all_formats = self.iso_formats + self.other_formats
    
    def __convert_unix_to_datetime(self, timestamp):
        # Convert Unix timestamp to datetime
        return datetime.utcfromtimestamp(timestamp)
      
      
    def convert_str_to_datetime(self, date_str):
        if not isinstance(date_str, str):
            return date_str

        if isinstance(date_str, int):
            return self.__convert_unix_to_datetime(date_str)

        date_str = date_str.strip()
        
        try:
          date_str = int(date_str)
          return self.__convert_unix_to_datetime(date_str)
        except:
          pass

        # Try ISO 8601 formats first
        for fmt in self.iso_formats:
            try:
                return datetime.strptime(date_str, fmt)
            except ValueError:
                continue
        
        # Try other common formats
        for fmt in self.other_formats:
            try:
                return datetime.strptime(date_str, fmt)
            except ValueError:
                continue
        
        # If direct parsing fails, try with dateutil parser
        try:
            return parser.isoparse(date_str)
        except ValueError:
            try:
              return dateparser.parse(date_str)
            except:
              pass 
            raise ValueError(f"Date string '{date_str}' is not in a recognized format")

    
      

class Transform:
  
    # START PRIVATE METHOD
    def __init__(self, path='root', separator='.', *args):
        self.path = path if len(path) > 0 else 'root'
        self.separator = separator if separator else '.'
        self.fields = args
    
    def __reduce(self, key, data):
      key_list = key.split(self.separator)
      
      try:
        return reduce(lambda d, k: d[k.replace(f':{k.split(":")[-1]}','')], key_list, data) if key_list else None
      except Exception as ex:
        return None
    
    def __get_default_value(self, type_):
      type__ = {
        'string': '',
        'int64': 0,
        'float': 0.0,
        'list': [],
        'object': {}
      }
      return type__[type_]
    
    def __set_blank_key(self):
      for i in range(len(self.result['rows'])):
        temp = len(self.result['rows'][i])
        if temp < len(self.result['columns']):
          for e in self.result['columns']:
            val, ok = self.result['rows'][i][e['name']]
            if not ok:
              self.result['rows'][i].update({e['name']:self.__get_default_value(e['type'])})
      
      return self
    
    def __get_columns(self):
      self.result['columns'] = [dict(t) for t in {tuple(d.items()) for d in [{'name': k, 'type': self.__get_value_type(v)} for field in self.result['rows'] for k, v in field.items()]}] 
      self.__change_default_value()
      self.result['columns'] = [elm for elm in self.result['columns'] if elm['type'] is not None]

      return self
    
    def __check_null(self):
      from collections import defaultdict
      
      name_counts = defaultdict(int)
      duplicate_names_data = []

      for item in self.result['columns']:
          name = item["name"]
          name_counts[name] += 1
      
      for k, v in name_counts.items():
        if v == 1:
          continue
        
        for item in self.result['columns']:
          if item['name'] == k and item['type']:
            duplicate_names_data.append(item)

      return duplicate_names_data

    def __change_default_value(self):
      duplicate = self.__check_null()
      
      for i in range(len(self.result['rows'])):
        for elm in duplicate:
          _name = elm['name']
          _type = elm['type'] 
          
          if self.__get_value_type(self.result['rows'][i][_name]) is None and _type:
            self.result['rows'][i] = self.__set_default_value(self.result['rows'][i], _name, _type)
      return self
    
    def __set_default_value(self, data, name_, type_):
      data[name_] = 0 if type_ == 'int64' else '' if type_ == 'string' else 0.0 if type_ == 'float' else [] if type_ == 'list' else {}
      return data
      
    def __get_value_type(self, value):
        if isinstance(value, int):
            return 'int64'
        elif isinstance(value, str):
            return 'string'
        elif isinstance(value, float):
            return 'float'
        elif isinstance(value, list):
            return 'list'
        elif isinstance(value, dict):
            return 'object'
        else:
            return None
    
    def __get_key_value(self, key, data):
        func    = key.split('|')
        new_key = func[-1].split(self.separator)[-1]

        if len(func) == 1:
            return {
                new_key.split(':')[-1] : self.__reduce(key, data)
            }
        
        new_key_func    = key.replace(f'{func[0]}|','')
        new_data        = self.__reduce(new_key_func, data)
        
        return {new_key_func.split(self.separator)[-1].split(':')[-1] : self.__exec(func[0], new_data) }
    
    def __concat(self, key, data):
      keys = key.split("=")
      keys = keys[-1].split('&')
      word = ""
      for key in keys:
          key = key.strip()
          word += reduce(lambda d, k: d[k], key.split(self.separator), data) if "'" not in key else key.replace("'", "")
      return word
    
    def __exec(self, func, data):
      child = func.split(':')
      check = 'index' in child[0] and len(child[0].split('=')) == 2
      index = int(child[0].split('=')[-1]) if check else None
      
      concat = 'concat' in child[0]
      if concat:
        return self.__concat(child[0].split('=')[-1], data)
      
      if len(child) == 2:
        data = [reduce(lambda d, k: d[k], child[-1].split(self.separator), elm) for elm in data] 
      
      numeric_data = [x for x in data if isinstance(x, (int, float))]
      
      
      if check and len(numeric_data) > 0:
        return numeric_data[index] if index is not None else data
      
      if child[0] == 'count':
          return len(data)
      if child[0] == 'average':
          return sum(numeric_data) / len(numeric_data) if len(numeric_data) > 0 else 0
      
      if child[0] == 'max':
        return max(numeric_data)
      
      if child[0] == 'min':
        return min(numeric_data)
      
      if child[0] == 'sum':
        return sum(numeric_data)
      
      return 0
    
    def __convert_unix_to_datetime(self, unix_timestamp):
      return datetime.fromtimestamp(unix_timestamp)
    
    def __convert_datetime_to_unix(self, datetime):
      if isinstance(datetime, str):
        datetime = self.__convert_str_to_datetime(datetime)
        return int(datetime.timestamp())
    
    def __convert_str_to_datetime(self, date_str):
      try:
          converter = DateConverter()
          return converter.convert_str_to_datetime(date_str)
      except ValueError:
          raise ValueError(f"Date string '{date_str}' is not in a recognized format")
      
    
    
    def __convert_str_to_unix(self, unix):
      
      try:
        unix = int(unix)
        return unix
      except:
        return self.__convert_datetime_to_unix(unix)
    
    def __int_to_ordinal(self, num):
      if num % 100 in [11, 12, 13]:  # Special cases for 11th, 12th, 13th
          suffix = "th"
      else:
          suffix = {1: "st", 2: "nd", 3: "rd"}.get(num % 10, "th")
      return f"{num}{suffix}"
        
        
      
    # END PRIVATE METHOD
    
    # Main Func
    def sort(self, field):
      self.result['rows'] = sorted(self.result['rows'], key=lambda x: x[field.replace('-','')], reverse='-' in field)
      return self
    
    def parse(self, payload):
        try:
          pl    = json.loads(re.sub(r'[^\x20-\x7E]', '', payload)) if isinstance(payload, str) else payload
        except Exception:
          pl    = json.loads(payload) if isinstance(payload, str) else payload
        pl      = reduce(lambda d, key: d[key], self.path.split(self.separator), pl) if self.path != 'root' else pl
        self.result  = { 
            'rows': [pl] if isinstance(pl, dict) else pl, 
            'columns': [dict(t) for t in {tuple(d.items()) for d in [{'name': k, 'type': self.__get_value_type(v)} for field in pl for k, v in field.items()]}] 
        }
        temps = []
        for i in range(len(self.result['rows'])):
          temp = self.result['rows'][i]
          if len(temp) < len(self.result['columns']):
            for e in self.result['columns']:
              name_ = e.get('name')
              type_ = e.get('type')
              
              if not self.result['rows'][i].get(name_, None):
                temp.update({name_:self.__get_default_value(type_)})
                # print(name_, type_)
          temps.append(temp)
          
        self.result['rows'] = temps
        
        if not self.fields:
            return self
        
        temp_rows = []
        for elm in self.result['rows']:
            temp = {}
            for key in self.fields: 
                temp.update(self.__get_key_value(key, elm))
            temp_rows.append(temp)
        self.result['rows'] = temp_rows
        self.__get_columns()
        
        return self

    def transform(self, *fields):
      if fields:
        temp_rows = []
        for elm in self.result['rows']:
            temp = {}
            for key in fields:
                temp.update(self.__get_key_value(key, elm))
            temp_rows.append(temp)
      
      self.result['rows'] = temp_rows
      self.__get_columns()
      
      return self
      # return json.dumps(self.result, indent=2)
    
    def to_json(self, indent=2):
      return json.dumps(self.result, indent=indent)
    
    
    # Util Func
    def convert_date(self, field, to_='dt'):
      # only to option to dt and unix
      for i in range(len(self.result['rows'])):
        
        val = self.result['rows'][i].get(field, None)
        if to_ == 'dt':  
          if val:
            self.result['rows'][i][field] = self.__convert_str_to_datetime(val)
            continue
        
        if val:
            self.result['rows'][i][field] = self.__convert_str_to_unix(val)

      return self
        
    def date_trunc(self, field, to, alias='', sort=''):
      
        entries = self.result['rows']
        
        # Convert date field if necessary
        self.convert_date(field, 'dt')
        self.sort(field)
        
        res = defaultdict(int)
        
        # Define date formats for truncation
        trunc_formats = {
            'second': '%Y-%m-%d %H:%M:%S',
            'minute': '%Y-%m-%d %H:%M',
            'hour': '%Y-%m-%d %H',
            'day': '%Y-%m-%d',
            'month': '%Y-%m',
            'year': '%Y',
            'week': lambda dt: f'{self.__int_to_ordinal(dt.isocalendar()[1])} week of {dt.isocalendar()[0]}'
        }
        
        # Process each entry
        for entry in entries:
            dt = entry[field]
            if to == 'week':
                trunc_key = trunc_formats['week'](dt)
            else:
                trunc_key = dt.strftime(trunc_formats[to])
            
            res[trunc_key] += 1
        
        # Prepare result in desired format
        self.result['rows'] = [{to: k, field if alias == '' else alias: v} for k, v in res.items()]
        self.__get_columns()
        
        if sort != '':
          self.sort()
          
        return self
      
    def average(self, group_by, field, alias='', sort=''):
      
      if field or field != '':
        temp = {}
        for elm in self.result['rows']:
          if elm[group_by] in temp:
            temp[elm[group_by]]['sum'] += elm[field]
            temp[elm[group_by]]['count'] += 1
          else:
            temp.update({elm[group_by] : {'sum': elm[field], 'count': 1}})
        
        self.result['rows'] = [{group_by: k, alias if alias else field: v['sum'] / v['count'] if v['sum'] > 0 else 0} for k, v in temp.items()]
      else:
        temps = []
        for elm in self.result['rows']:
          temp = {group_by: elm[group_by], alias:0}
          for k, v in elm.items():
            if k != group_by:
              try:
                temp[alias] += int(v)
              except:
                temp[alias] = 0
          temp[alias] = temp[alias] / (len(elm) - 1)
          temps.append(temp)
        self.result['rows'] = temps
      
      self.__get_columns()
        
      if sort != '':
        self.sort(sort)
      
      return self

    def sum(self, group_by, field, alias='', sort=''):
      
      if field or field != '':
        temp = {}
        for elm in self.result['rows']:
          if elm[group_by] in temp:
            temp[elm[group_by]] += elm[field]
          else:
            temp.update({elm[group_by] : elm[field]})
            
        self.result['rows'] = [{group_by: k, alias if alias else 'sum': v} for k, v in temp.items()]
      else:
        temps = []
        for elm in self.result['rows']:
          temp = {group_by: elm[group_by], alias:0}
          for k, v in elm.items():
            if k != group_by:
              try:
                temp[alias] += int(v)
              except:
                temp[alias] = 0
          temps.append(temp)
        self.result['rows'] = temps
        
      self.__get_columns()
        
      if sort != '':
        self.sort(sort)
      
      return self
    
    def max(self, group_by, field, alias='', sort=''):
      if field or field != '':
        temp = {}
        for elm in self.result['rows']:
          if elm[group_by] in temp:
            temp[elm[group_by]] = elm[field] if elm[field] > temp[elm[group_by]] else temp[elm[group_by]]
          else:
            temp.update({elm[group_by] : elm[field]})
            
        self.result['rows'] = [{group_by: k, alias if alias else field: v} for k, v in temp.items()]
      else:
        temps = []
        for elm in self.result['rows']:
          temp = {group_by: elm[group_by], alias:0}
          for k, v in elm.items():
            if k != group_by:
              if temp[alias] == 0:
                temp[alias] = v
              if v > temp[alias]:
                temp[alias] = v
          temps.append(temp)
        self.result['rows'] = temps
          
      self.__get_columns()
        
      if sort != '':
        self.sort(sort)
      
      return self
    
    def min(self, group_by, field, alias='', sort=''):
      if field or field != '':
        temp = {}
        for elm in self.result['rows']:
          if elm[group_by] in temp:
            temp[elm[group_by]] = elm[field] if elm[field] < temp[elm[group_by]] else temp[elm[group_by]]
          else:
            temp.update({elm[group_by] : elm[field]})
            
        self.result['rows'] = [{group_by: k, alias if alias else field: v} for k, v in temp.items()]
      else:
        temps = []
        for elm in self.result['rows']:
          temp = {group_by: elm[group_by], alias:0}
          for k, v in elm.items():
            if k != group_by:
              if temp[alias] == 0:
                temp[alias] = v
              if v < temp[alias]:
                temp[alias] = v
          temps.append(temp)
        self.result['rows'] = temps
          
      self.__get_columns()
        
      if sort != '':
        self.sort(sort)
      
      return self
    
    def mod(self, field, by=0, name='mod', sort=''):
      
      self.result['rows'] = [{name: elm[field] % by, **elm} for elm in self.result['rows']]
      self.__get_columns()
      
      if sort != '':
        self.sort(sort)
        
      return self
    
    def pow(self, field, by=0, name='pow', sort=''):
      
      self.result['rows'] = [{name: elm[field] ** by, **elm} for elm in self.result['rows']]
      self.__get_columns()
      
      if sort != '':
        self.sort(sort)
        
      return self

    def ceil(self, field, name='ceil', sort=''):
      
      self.result['rows'] = [{name: math.ceil(elm[field]), **elm} for elm in self.result['rows']]
      self.__get_columns()
      
      if sort != '':
        self.sort(sort)
        
      return self
    
    def floor(self, field, name='floor', sort=''):
      
      self.result['rows'] = [{name: math.floor(elm[field]), **elm} for elm in self.result['rows']]
      self.__get_columns()
      
      if sort != '':
        self.sort(sort)
        
      return self
    
    def concat(self, field1, field2, separator=' ', name='concat', sort=''):
      
      self.result['rows'] = [{name: f'{elm[field1]}{separator}{elm[field2]}', **elm} for elm in self.result['rows']]
      self.__get_columns()
      
      if sort != '':
        self.sort(sort)
        
      return self
    
    def len(self, field, name='len', sort=''):
      
      self.result['rows'] = [{name: len(elm[field]), **elm} for elm in self.result['rows']]
      self.__get_columns()
      
      if sort != '':
        self.sort(sort)
        
      return self
    
    def replace(self, field, index=0, length=1, value='', name='replace', sort=''):
      
      self.result['rows'] = [{name: elm[field][:index] + value + elm[field][index+length:], **elm} for elm in self.result['rows']]
      self.__get_columns()
      
      if sort != '':
        self.sort(sort)
        
      return self
    
    def upper(self, field, name='upper', sort=''):
      
      self.result['rows'] = [{name: elm[field].upper(), **elm} for elm in self.result['rows']]
      self.__get_columns()
      
      if sort != '':
        self.sort(sort)
        
      return self
    
    def lower(self, field, name='lower', sort=''):
      
      self.result['rows'] = [{name: elm[field].lower(), **elm} for elm in self.result['rows']]
      self.__get_columns()
      
      if sort != '':
        self.sort(sort)
        
      return self
    
    def title(self, field, name='title', sort=''):
      
      self.result['rows'] = [{name: elm[field].title(), **elm} for elm in self.result['rows']]
      self.__get_columns()
      
      if sort != '':
        self.sort(sort)
        
      return self

class TransformDocx:

    """
    from transform import Transform, TransformDocx
    
    docs = TransformDocx('https://docs.google.com/spreadsheets/d/134j1g85pjBkIk6f6DCoVJN3SfE5gTM_Dn_h9yTPzI3U/edit#gid=0', 'Sheet1')
    ok, docs = docs.transform(4, ["no", "name", "age", "test"])

    print(docs)
    """
    
    def __init__(self, path: str, sheet_name: str):
        self.path = path
        self.sheet_name = sheet_name

        # Check if the file extension is supported
        if self.__is_excel_file():
            success, self.path = self.__convert_excel_to_csv_in_memory()
            if not success:
                raise ValueError("File conversion failed. Unable to read the file. " + self.path)

    def __is_excel_file(self):
        # Check if the file has a supported Excel extension
        supported_extensions = ["xls", "xlsx"]
        file_extension = self.path.split(".")[-1].lower()
        return file_extension in supported_extensions

    def __is_spreadsheet(self):
        try:
            if self.path.find("https://") != -1:
                return True
            else:
                return False
        except:
            return False

    def __convert_excel_to_csv_in_memory(self):
        try:
            # Read the Excel file
            df = pd.read_excel(self.path, sheet_name=self.sheet_name)

            # Convert DataFrame to CSV in memory
            csv_content = StringIO()
            df.to_csv(csv_content, index=False)
            csv_content.seek(0)  # Move the cursor to the beginning of the stream

            return True, csv_content

        except Exception as e:
            return False, f"Conversion failed. Error: {str(e)}"

    def __load_spreadsheet_to_csv_in_memory(self, skiprows=0):
        key         = os.getenv('GCLOUD_KEY', 'key.json')
        credentials = service_account.Credentials.from_service_account_file(key, scopes=['https://www.googleapis.com/auth/spreadsheets'])
        try:
            gc          = gspread.authorize(credentials)
            spreadsheet = gc.open_by_url(self.path)
            worksheet   = spreadsheet.worksheet(self.sheet_name)
            values      = worksheet.get_all_values()[skiprows:]
            df          = pd.DataFrame(values)
            
            csv_content = StringIO()
            
            df.to_csv(csv_content, index=False)
            csv_content.seek(0)  # Move the cursor to the beginning of the stream

            return True, csv_content

        except Exception as e:
            return False, f"Conversion failed. Error: {str(e)}"
         
    def transform(self, skiprows, usecols):
        try:
            # Try reading the file as a CSV file and skip the specified rows
            check = self.__is_spreadsheet()
            if check:
                ok, self.path = self.__load_spreadsheet_to_csv_in_memory(skiprows)
                if not ok:
                    return ok, self.path
                
            df = pd.read_csv(self.path, header=skiprows if not check else 1, usecols=usecols)

            # Get column information
            columns_info = [{"name": col, "type": str(df[col].dtype)} for col in df.columns]

            # Convert DataFrame to JSON format
            rows_data = df.to_dict(orient='records')
            json_output = {"columns": columns_info, "rows": rows_data}

            return True, json.dumps(json_output, indent=2)

        except Exception as e:
            return False, str(e)
    
    def convert(self, skiprows, usecols):
        
        check, data = self.transform(skiprows=skiprows, usecols=usecols)
        if not check:
            return check, data
        
        return True, Transform('rows', '.').parse(data)
        

class Merge:
  
  """Example
  
  with open('/home/dayat/WORK/python/dashboard-ex/media/query/f4dd9f94-c310-4cfd-8649-e23f71d417b4/result.json', 'r') as em:
    employee = json.loads(em.read())

  with open('/home/dayat/WORK/python/dashboard-ex/media/query/53707304-1991-48d7-8cfc-41fb870a48f9/result.json', 'r') as w:
      workunit = json.loads(w.read())

  with open('/home/dayat/WORK/python/dashboard-ex/media/query/da414c9a-22d4-4d91-8180-abcccf2b0a0a/result.json', 'r') as l:
      workunit_level = json.loads(l.read())


  wu = Merge().Select(*[
      ['workunit.id', 'id'],
      ['workunit.name', 'name'],
      ['workunit_level.name', 'workunit_level']
  ]).From(name='workunit', data=workunit).Join(name='workunit_level', data=workunit_level, condition='workunit.workunit_level_id=workunit_level.id').parse()

  cfg = Merge().Select(*[
      ['employee.id', 'id'],
      ['employee.name', 'name'],
      ['employee.email', 'email'],
      ['workunit.name', 'workunit'],
      ['workunit.workunit_level', 'workunit_level']
  ]).From(name='employee', data=employee).Join(name='workunit', data=wu.results, condition='workunit.id=employee.workunit_id').parse()
  print(cfg.to_json())
  
  """
  
  def __init__(self, *args, **kwargs):
      self.data = {}
      self.results = {
        'rows': [],
        'columns': []
      }
      self.joins = {}
  
  def __get_value_type(self, value):
      if isinstance(value, int):
          return 'int64'
      elif isinstance(value, str):
          return 'string'
      elif isinstance(value, float):
          return 'float'
      elif isinstance(value, list):
          return 'list'
      elif isinstance(value, dict):
          return 'object'
      else:
          return None
  
  def __get_default_value(self, type_):
      type__ = {
        'string': '',
        'int64': 0,
        'float': 0.0,
        'list': [],
        'object': {}
      }
      return type__[type_]
  
  def __check_null(self):
      from collections import defaultdict
      
      name_counts = defaultdict(int)
      duplicate_names_data = []

      for item in self.results['columns']:
          name = item["name"]
          name_counts[name] += 1
      
      for k, v in name_counts.items():
        if v == 1:
          continue
        
        for item in self.results['columns']:
          if item['name'] == k and item['type']:
            duplicate_names_data.append(item)

      return duplicate_names_data
    
  def __set_default_value(self, data, name_, type_):
      data[name_] = 0 if type_ == 'int64' else '' if type_ == 'string' else 0.0 if type_ == 'float' else [] if type_ == 'list' else {}
      return data
    
  def __change_default_value(self):
      duplicate = self.__check_null()
      
      for i in range(len(self.results['rows'])):
        for elm in duplicate:
          _name = elm['name']
          _type = elm['type'] 
          
          if self.__get_value_type(self.results['rows'][i][_name]) is None and _type:
            self.results['rows'][i] = self.__set_default_value(self.results['rows'][i], _name, _type)
      return self
    
  def __get_columns(self):
      self.results['columns'] = [dict(t) for t in {tuple(d.items()) for d in [{'name': k, 'type': self.__get_value_type(v)} for field in self.results['rows'] for k, v in field.items()]}] 
      self.__change_default_value()
      self.results['columns'] = [elm for elm in self.results['columns'] if elm['type'] is not None]

      return self
  
  def __get_data(self):
      try:
        for elm in self.from_data[self.from_data['name']]['rows']:
            # print(elm)
            temp = {}
            for key, alias in self.select:
                keys = key.split('.')
                if self.from_data['name'] == keys[0]:
                  temp.update({alias if alias != '' else keys[-1]: elm[keys[-1]]})
                else:
                  for w in self.joins[keys[0]]['data']['rows']:
                    con1, con2 = self.joins[keys[0]]['condition'].split('=')
                    con1 = w[con1.split('.')[-1]] if con1.split('.')[0] == keys[0] else elm[con1.split('.')[-1]]
                    con2 = w[con2.split('.')[-1]] if con2.split('.')[0] == keys[0] else elm[con2.split('.')[-1]]
                    if con1 == con2:
                      temp.update({alias if alias != '' else keys[-1] : w[keys[-1]]})
                      break
                    else:
                      try:
                        if int(con1) == int(con2):
                          temp.update({alias if alias != '' else keys[-1] : w[keys[-1]]})
                          break
                      except:
                        pass
                    
            self.results['rows'].append(temp)
        self.__get_columns()
      except Exception as ex:
          print(ex)
  
  def parse(self):
      self.__get_data()
      return self
      
  def to_json(self, indent=2):
    return json.dumps(self.results, indent=2)
              
  def Select(self, *args):
      self.select = args
      return self
  
  def From(self, **data):
      self.from_data = {
          'name' : data['name'],
          data['name'] : data['data']
      }
      return self
  
  def Join(self, **kwargs):
      self.joins.update({kwargs['name']: kwargs})
      return self


