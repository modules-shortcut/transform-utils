import requests,json, random

class Proxy:
    
    def __init__(self, user='', password='', *hosts):
        self.user = 'geonode_kouSFtZJfj' if user == '' else user
        self.password = '8eabb88c-73cc-4e0b-b1fa-6cbe042c4f4c' if password == '' else password
        self.hosts = [
                'premium-residential.geonode.com:9000',
                'premium-residential.geonode.com:9001',
                'premium-residential.geonode.com:9002',
                'premium-residential.geonode.com:9003',
                'premium-residential.geonode.com:9004',
                'premium-residential.geonode.com:9005',
                'premium-residential.geonode.com:9006',
                'premium-residential.geonode.com:9007',
                'premium-residential.geonode.com:9008',
                'premium-residential.geonode.com:9009',
                'premium-residential.geonode.com:9010',
                'us.premium-residential.geonode.com:9000',
                'us.premium-residential.geonode.com:9001',
                'us.premium-residential.geonode.com:9002',
                'us.premium-residential.geonode.com:9002',
                'us.premium-residential.geonode.com:9003',
                'us.premium-residential.geonode.com:9004',
                'us.premium-residential.geonode.com:9005',
                'us.premium-residential.geonode.com:9006',
                'us.premium-residential.geonode.com:9007',
                'us.premium-residential.geonode.com:9008',
                'us.premium-residential.geonode.com:9009',
                'us.premium-residential.geonode.com:9010',
                'sg.premium-residential.geonode.com:9000',
                'sg.premium-residential.geonode.com:9001',
                'sg.premium-residential.geonode.com:9002',
                'sg.premium-residential.geonode.com:9003',
                'sg.premium-residential.geonode.com:9004',
                'sg.premium-residential.geonode.com:9005',
                'sg.premium-residential.geonode.com:9006',
                'sg.premium-residential.geonode.com:9007',
                'sg.premium-residential.geonode.com:9008',
                'sg.premium-residential.geonode.com:9009',
                'sg.premium-residential.geonode.com:9010',
            ] if len(hosts) == 0 else list(hosts)
        
        self.proxies = [{'http': f'http://{self.user}:{self.password}@{host}', 'https': f'http://{self.user}:{self.password}@{host}'} for host in self.hosts]
    
    def __get_proxy(self):
        seq = random.randint(0,len(self.proxies)-1)
        return self.proxies[seq]
    
    def __get_header(self):
        file        = open('user-agents.txt', 'r')
        data        = file.read()
        user_agents = data.split('\n')
        seq         = random.randint(0, len(user_agents)-1)
        return {
            'User-Agent': user_agents[seq]
        }
    
    def test(self, url):
        try:
            p   = self.__get_proxy()
            res = requests.get(url, proxies=p, headers=self.__get_header(), timeout=10, verify=True)
            if res.status_code == 200:
                print("=====================================================")
                print(f"Proxy {p} is working")
                print(f"Response from IP API: ")
                print(res.text)
                print("=====================================================")
            else:
                print(f"Proxy {p} returned status code: {res.status_code}")
        except requests.exceptions.HTTPError as errh:
            print(f"HTTP Error for proxy {p}: {errh}")
        except requests.exceptions.ProxyError as errp:
            print(f"Proxy Error for proxy {p}: {errp}")
        except requests.exceptions.ConnectionError as errc:
            print(f"Error Connecting for proxy {p}: {errc}")
        except requests.exceptions.Timeout as errt:
            print(f"Timeout Error for proxy {p}: {errt}")
        except requests.exceptions.RequestException as err:
            print(f"Other Request Exception for proxy {p}: {err}")


p = Proxy()

for i in range(100):
    p.test('https://proxy-test.underdev.team/')

