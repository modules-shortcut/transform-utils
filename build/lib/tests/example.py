from transform import Transform, TransformDocx\
    
docs = TransformDocx('https://docs.google.com/spreadsheets/d/134j1g85pjBkIk6f6DCoVJN3SfE5gTM_Dn_h9yTPzI3U/edit#gid=0', 'Sheet1')
ok, docs = docs.convert(4, ["no", "name", "age", "test"])

print(docs.transform('name', 'age').to_json())