import json
from functools import reduce
from transform import Transform 

# Sample JSON payload for testing
sample_payload = """
{
  "data": [
    {"name": "John", "age": 25, "salary": 50000},
    {"name": "Jane", "age": 30, "salary": 60000},
    {"name": "Bob", "age": 35, "salary": 70000}
  ]
}
"""

def test_transform_class():
    keys = [
        'name:nama',
        'age:umur',
        'salary:gaji'
    ]
    # Instantiate the Config class
    config = Transform('data', '.', *keys)

    # Test the parse method
    config.parse(sample_payload)

    # Test the transform method
    config.transform('nama', 'umur', 'gaji')

    # Test the to_json method
    result_json = config.to_json()
    print("Result JSON:")
    print(result_json)

    # Additional tests for aggregation functions (count, average, sum, max, min)
    # config.count(group_by='age', alias='age_count', sort='age_count')
    # config.average(group_by='age', field='salary', alias='avg_salary', sort='avg_salary')
    # config.sum(group_by='age', field='salary', alias='total_salary', sort='total_salary')
    # config.max(group_by='age', field='salary', alias='max_salary', sort='max_salary')
    # config.min(group_by='age', field='salary', alias='min_salary', sort='min_salary')

    # # Additional tests for mathematical functions (mod, pow)
    # config.mod(field='age', by=2, name='age_mod', sort='age_mod')
    # config.pow(field='age', by=2, name='age_pow', sort='age_pow')

    # # Additional tests for string functions (concat, len, replace, upper, lower, title)
    # config.concat(field1='name', field2='age', separator='_', name='name_age_concat', sort='name_age_concat')
    # config.len(field='name', name='name_length', sort='name_length')
    # config.replace(field='name', index=1, length=2, value='XYZ', name='name_replace', sort='name_replace')
    # config.upper(field='name', name='name_upper', sort='name_upper')
    # config.lower(field='name', name='name_lower', sort='name_lower')
    # config.title(field='name', name='name_title', sort='name_title')

    # Test the sort method
    config.sort(field='nama')

    # Test the get_columns method
    columns = config.get_columns()

    # Test the to_json method after all transformations
    final_result_json = config.to_json()
    print("Final Result JSON:")
    print(final_result_json)

if __name__ == "__main__":
    test_transform_class()
