# Transform Class

The `Transform` class is a Python utility for performing data transformations on structured data.
It provides a set of methods to manipulate, aggregate, and derive new data based on the given input.
The class is designed to be flexible and extensible, allowing users to chain multiple transformation operations.

## Table of Contents

1. [Installation](#installation)
2. [Usage](#usage)
    - [Initialization](#initialization)
    - [Data Parsing](#data-parsing)
    - [Transformation Operations](#transformation-operations)
3. [Examples](#examples)
4. [Methods](#methods)
    - [Common Methods](#common-methods)
    - [Transformation Methods](#transformation-methods)
5. [Contributing](#contributing)
6. [License](#license)

## Installation

```bash
pip install transform-utils
```

## Usage

### Initialization

```python
from transform import Transform

# Initialize Transform instance
transformer = Transform(path='root', separator='.', field1, field2, ...)
```

### Data Parsing

```python
# Parse JSON payload
transformer.parse(json_payload)

# Access transformed data
result = transformer.result
```

### Transformation Operations

```python
# Perform transformations
transformer.transform(field1, field2, ...).sort('column_name').to_json()
```

## Examples

```json
# example json data witch wan to transform
{
    "page": 1,
    "page_size": 10,
    "total_page": 8,
    "next": "http://edm.underdev.team/api/management/employee/?page=2",
    "previous": null,
    "count": 77,
    "results": [
        {
            "id": 82,
            "name": "NAMA LENKAP SAYA2",
            "nip": "19701231 199803 1 0299",
            "nrp": "00000000",
            "email": "emaillenkapsaya@gmail.com",
            "address": "alamat",
            "phone_number": "081234567123",
            "photo": "/api/drive/source/36ef89a6f19dd9d1f950c1cfaefde1a4de",
            "user_id": 126,
            "user": 126,
            "classes_id": 4,
            "classes": {
                "id": 4,
                "name": "GOLONGAN IV",
                "slug": "golongan-iv",
                "description": null,
                "created_at": "2023-08-17T08:51:42.403000+07:00",
                "updated_at": "2023-08-17T08:51:42.403000+07:00"
            },
            "grade_id": 27,
            "grade": {
                "id": 27,
                "name": "JAKSA AGUNG",
                "slug": "jaksa-agung",
                "grade_type": null,
                "description": "JAKSA AGUNG",
                "classes_id": null,
                "classes": null,
                "created_at": "2023-08-17T09:11:12.766000+07:00",
                "updated_at": "2023-08-17T09:11:12.766000+07:00"
            },
            "position_id": 356,
            "position": {
                "id": 356,
                "name": "PENGELOLA SISTEM DAN JARINGAN",
                "slug": null,
                "position_type": 2,
                "description": "-",
                "sector_id": 1,
                "sector": {
                    "id": 1,
                    "name": "INTELIJEN",
                    "slug": "intelijen",
                    "description": "INTELIJEN",
                    "created_at": null,
                    "updated_at": null,
                    "parent": null,
                    "work_unit_level": []
                },
                "work_unit_level_id": 3,
                "work_unit_level": {
                    "id": 3,
                    "name": "KEJAKSAAN NEGERI",
                    "slug": "kejaksaan-negeri",
                    "description": "KEJAKSAAN NEGERI",
                    "created_at": "2023-08-17T08:19:23.745000+07:00",
                    "updated_at": "2023-08-17T08:19:23.745000+07:00"
                },
                "parent_id": null,
                "parent": null,
                "code": null,
                "children": [],
                "created_at": "2023-09-22T00:41:45+07:00",
                "updated_at": "2023-09-22T00:41:45+07:00"
            },
            "sector": {
                "id": 1,
                "name": "INTELIJEN",
                "slug": "intelijen",
                "description": "INTELIJEN",
                "created_at": null,
                "updated_at": null,
                "parent": null,
                "work_unit_level": []
            },
            "sector_id": 1,
            "work_unit_id": 464,
            "work_unit": {
                "id": 464,
                "name": "PAPUA BARAT",
                "slug": null,
                "code": "00001",
                "address": "Jl. Pahlawan, Sanggeng, Kec. Manokwari Bar., Kabupaten Manokwari, Papua Bar. 98312",
                "phone_number": "-",
                "logo": "https://apis.siaccinfo.id/drive/static/logo/bG9nby1rZWpha3NhYW4ucG5n?v=H4sIAAAAAAAA_zIzSjUwSjJKMk0xsEg1NjdKNDIzSTI0SwUEAAD__0G3U8wYAAAA",
                "latitude": -0.865086946292935,
                "longitude": 134.057897458601,
                "description": "-",
                "work_unit_level_id": 2,
                "work_unit_level": {
                    "id": 2,
                    "name": "KEJAKSAAN TINGGI",
                    "slug": "kejaksaan-tinggi",
                    "description": "KEJAKSAAN TINGGI",
                    "created_at": "2023-08-17T08:18:55.990000+07:00",
                    "updated_at": "2023-08-17T08:18:55.990000+07:00"
                },
                "parent_id": 1,
                "parent": 1,
                "created_at": "2023-12-12T18:48:10.111000+07:00",
                "updated_at": "2023-12-12T18:48:10.111000+07:00"
            },
            "role_id": 1,
            "role": {
                "id": 1,
                "name": "Operator",
                "role": 1,
                "role_display": "Operator",
                "slug": "operator-operator",
                "is_active": true
            },
            "code": 2,
            "code_id": 2,
            "code_subdir": 7,
            "code_subdir_id": 7,
            "code_seksi": 27,
            "code_seksi_id": 27,
            "code_sector": 67,
            "code_sector_id": 67,
            "signer": false,
            "created_at": "2023-11-29T12:09:08.653000+07:00",
            "updated_at": "2023-12-19T11:47:39.004000+07:00"
        }
    ]
}
```

```python
from transform import Transform

transform = Transform(
    path='results', 
    separator='.', 
    'id:nomor', 
    'name:nama', 
    'classes.name:golongan',
    'grade.name:pangkat',
    'work_unit.name:nama_satuan_kerja',
    'work_unit.work_unit_level:level_satuan_kerja',
    'role.name:role'
).parse(data_json)

transform = transform.concat(
    field1='level_satuan_kerja', 
    field2='nama_satuan_kerja', 
    separator=' ', 
    name='satuan_kerja'
)
transform = transform.concat(
    field1='pangkat', 
    field2='golongan', 
    separator='/', 
    name='pangkat_golongan'
)

transform = transform.transform(
    'nomor',
    'nama',
    'pangkat_golongan',
    'satuan_kerja',
    'role'
)

result = transform.to_json()
print(result)


```

```json
#result printed
{
    "rows": [
        {
            "nomor": 82,
            "nama": "NAMA LENKAP SAYA2",
            "pangkat_golongan": "JAKSA AGUNG/GOLONGAN IV",
            "satuan_kerja": "KEJAKSAAN TINGGI PAPUA BARAT",
            "role": "Operator"
        }
    ],
    "columns": [
        {
            "name": "nomor",
            "type": "integer",
        },
        {
            "name": "nama",
            "type": "string",
        },
        {
            "name": "pangkat_golonga",
            "type": "string",
        },
        {
            "name": "satuan_kerja",
            "type": "string",
        },
        {
            "name": "role",
            "type": "string",
        }
    ]
}
```

## Methods

### Common Methods

- `__init__(self, path='root', separator='.', *args)`: Initialize the Transform instance.
    - Parameters:
    - `path`: Root path in the input data (default is 'root').
    - `separator`: Separator used in field paths (default is '.').
    - `args`: List of field names to be selected for transformation.

- `parse(self, payload) -> Transform`: Parse and set the input data for transformation.
    - Parameters:
    - `payload`: JSON data or string to be parsed.
    - Returns:
    - `Transform`: Instance of the Transform class.

- `to_json(self, indent=2) -> str`: Convert the transformed data to JSON format.
    - Parameters:
    - `indent`: Number of spaces to use for indentation (default is 2).
    - Returns:
    - `str`: JSON-formatted string.

### Transformation Methods

- `transform(self, *fields) -> Transform`: Apply transformations to selected fields.
    - Parameters:
    - `fields`: List of field names to be transformed.
    - Returns:
    - `Transform`: Instance of the Transform class.

- `count(self, group_by, alias='', sort='') -> Transform`: Count occurrences based on a grouping field.
    - Parameters:
    - `group_by`: Field for grouping.
    - `alias`: Alias for the count column (default is '').
    - `sort`: Field to sort by (default is '').
    - Returns:
    - `Transform`: Instance of the Transform class.

- `average(self, group_by, field, alias='', sort='') -> Transform`: Calculate the average value based on a grouping field.
    - Parameters:
    - `group_by`: Field for grouping.
    - `field`: Field for calculating the average.
    - `alias`: Alias for the result column (default is '').
    - `sort`: Field to sort by (default is '').
    - Returns:
    - `Transform`: Instance of the Transform class.

- `sum(self, group_by, field, alias='', sort='') -> Transform`: Sum values based on a grouping field.
    - Parameters:
    - `group_by`: Field for grouping.
    - `field`: Field for summing.
    - `alias`: Alias for the sum column (default is '').
    - `sort`: Field to sort by (default is '').
    - Returns:
    - `Transform`: Instance of the Transform class.

- `max(self, group_by, field, alias='', sort='') -> Transform`: Find the maximum value based on a grouping field.
    - Parameters:
    - `group_by`: Field for grouping.
    - `field`: Field for finding the maximum.
    - `alias`: Alias for the result column (default is '').
    - `sort`: Field to sort by (default is '').
    - Returns:
    - `Transform`: Instance of the Transform class.

- `min(self, group_by, field, alias='', sort='') -> Transform`: Find the minimum value based on a grouping field.
    - Parameters:
    - `group_by`: Field for grouping.
    - `field`: Field for finding the minimum.
    - `alias`: Alias for the result column (default is '').
    - `sort`: Field to sort by (default is '').
    - Returns:
    - `Transform`: Instance of the Transform class.

- `mod(self, field, by=0, name='mod', sort='') -> Transform`: Apply modulo operation on a field.
    - Parameters:
    - `field`: Field for applying the modulo operation.
    - `by`: Divisor for modulo operation (default is 0).
    - `name`: Name for the result column (default is 'mod').
    - `sort`: Field to sort by (default is '').
    - Returns:
    - `Transform`: Instance of the Transform class.

- `pow(self, field, by=0, name='pow', sort='') -> Transform`: Apply power operation on a field.
    - Parameters:
    - `field`: Field for applying the power operation.
    - `by`: Exponent for power operation (default is 0).
    - `name`: Name for the result column (default is 'pow').
    - `sort`: Field to sort by (default is '').
    - Returns:
    - `Transform`: Instance of the Transform class.

- `ceil(self, field, name='ceil', sort='') -> Transform`: Apply ceil operation on a numeric field.
    - Parameters:
    - `field`: Numeric field for applying the ceil operation.
    - `name`: Name for the result column (default is 'ceil').
    - `sort`: Field to sort by (default is '').
    - Returns:
    - `Transform`: Instance of the Transform class.

- `floor(self, field, name='floor', sort='') -> Transform`: Apply floor operation on a numeric field.
    - Parameters:
    - `field`: Numeric field for applying the floor operation.
    - `name`: Name for the result column (default is 'floor').

## Contributing

If you'd like to contribute to this project, please follow the [contribution guidelines](CONTRIBUTING.md).

## License

This project is licensed under the [MIT License](LICENSE).
